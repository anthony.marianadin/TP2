function recherche(){
    var result;
    var searchValue = document.getElementById("search").value;
    if(searchValue == null)
    {
    	alert("aucune valeur à été trouvé");
    }
    var req = new XMLHttpRequest();
	req.open('GET', 'http://localhost:1234/api/?action=opensearch&format=json&search='+searchValue, true);
	req.onreadystatechange = function (aEvt) {
	  if (req.readyState == 4) {
	     if(req.status == 200)
	     {
	     	result = JSON.parse(req.responseText);
	     	//console.log(result);
	     	result[3].forEach(function(element) {
				var para = document.createElement("p");
				var node = document.createTextNode(element);
				para.appendChild(node);
				var divPrincipal = document.getElementById("Resultat");
				divPrincipal.appendChild(para);
			});
	     }
	     else
	     {
	      console.log("Erreur pendant le chargement de la page.\n");	     	
	     }
	  }
	};
	req.send(null);
}